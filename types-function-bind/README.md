# Installation
> `npm install --save @types/function-bind`

# Summary
This package contains type definitions for function-bind (https://github.com/Raynos/function-bind).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/function-bind.

### Additional Details
 * Last updated: Tue, 07 Nov 2023 03:09:37 GMT
 * Dependencies: none

# Credits
These definitions were written by [ExE Boss](https://github.com/ExE-Boss), and [Jordan Harband](https://github.com/ljharb).
